namespace Sila2.Examples.HelloSila.ClientApp
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using CommandLine;
    using Common.Logging;
    using Grpc.Core;

    using Discovery;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Program>();
            try
            {
                Channel channel;
                ClientCmdLineArgs options = null;
                Parser.Default.ParseArguments<ClientCmdLineArgs>(args)
                    .WithParsed(o => options = o)
                    .WithNotParsed((errs) => ClientCmdLineArgs.HandleParseError(errs));

                if (options.IpAddress == null && options.Port == -1)
                {
                    // use SiLA Server Discovery to find the HelloSila Server
                    const string namePattern = "HelloSila Server";
                    var server = SiLADiscovery.GetServers(3000).FirstOrDefault(s => s.Config.Name == namePattern);
                    if (server == null)
                    {
                        throw new InvalidOperationException($"No server named: {namePattern} found");
                    }

                    channel = server.Channel;
                }
                else
                {
                    channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(options.IpAddress), options.Port);
                }

                // create client
                var client = new Client(channel);

                // command SayHello
                var param = "SiLA user";
                Console.WriteLine($"Calling \"SayHello(\"{param}\")\" ...");
                Console.WriteLine(client.SayHello($"{param}"));

                // property CurrentYear
                Console.WriteLine("Calling \"StartYear()\" ...");
                Console.WriteLine(client.GetStartYear());

                // check range validation
                const double targetTemperatureOutOfRangeKelvin = 373.15;
                Console.WriteLine($"Calling \"ControlTemperature({targetTemperatureOutOfRangeKelvin})\" ...");
                client.ControlTemperature(targetTemperatureOutOfRangeKelvin).Wait();

                // start temperature control and getting the result without waiting for the execution to be finished
                const double targetTemperatureCelsius = 30;
                Console.WriteLine($"Calling \"ControlTemperature({targetTemperatureCelsius} °C)\" and ControlTemperature_Result() right afterwards");
                client.ControlTemperatureNoWait(UnitConverter.DegreeCelsius2Kelvin(targetTemperatureCelsius));

                // start temperature control with valid conditions
                Console.WriteLine($"Calling \"ControlTemperature({targetTemperatureCelsius} °C)\" ...");
                client.ControlTemperature(UnitConverter.DegreeCelsius2Kelvin(targetTemperatureCelsius), false).Wait();

                // subscribe to temperature property for 5 seconds
                Console.WriteLine("Calling \"Subscribe_CurrentTemperature()\" for 5 seconds ...");
                client.GetCurrentTemperature(new TimeSpan(0, 0, 5)).Wait();

                Thread.Sleep(1000);

                // subscribe to temperature property for receiving 3 values
                Console.WriteLine("Calling \"Subscribe_CurrentTemperature()\" for receiving 3 values ...");
                client.GetCurrentTemperature(3).Wait();

                Thread.Sleep(1000);

                // start another temperature control with running property outputs
                Console.WriteLine("Calling \"ControlTemperature(45 °C)\" ...");
                client.ControlTemperature(UnitConverter.DegreeCelsius2Kelvin(45)).Wait();

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                log.Error("Client Error: ", e);
                return;
            }

            Console.ReadKey();
        }
    }
}