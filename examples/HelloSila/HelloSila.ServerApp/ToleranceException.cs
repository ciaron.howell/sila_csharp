using System;
using System.Runtime.Serialization;

namespace Sila2.Examples.HelloSila.ServerApp
{
    [Serializable]
    public class ToleranceException : Exception
    {
        public ToleranceException()
        {
        }

        public ToleranceException(string message) : base(message)
        {
        }

        public ToleranceException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ToleranceException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}