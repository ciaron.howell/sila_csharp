﻿using System.Threading;
using Sila2.Utils;

namespace Sila2.Examples.HelloSila.ServerApp
{
    using System;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Sila2.Server;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : SiLA2Server, IDisposable
    {
        private readonly GreetingProviderImpl _greetingProviderImpl;
        private readonly TemperatureControllerImpl _temperatureControllerImpl;
        private static ILog Log = LogManager.GetLogger<Server>();

        public Server(int portNumber, NetworkInterface networkInterface, string configFile)
        : base(new ServerInformation(
                "HelloSila Server",
                "HelloSila Server that implements the GreetingsProvider and the TemperatureController feature",
                "www.equicon.de",
                "1.0"),
                portNumber,
                networkInterface,
                configFile)
        {
            // add GreetingProvider feature implementation
            ReadFeature("features/GreetingProvider.sila.xml");
            _greetingProviderImpl = new GreetingProviderImpl();
            GrpcServer.Services.Add(
                Sila2.Org.Silastandard.Examples.Greetingprovider.V1.GreetingProvider
                    .BindService(_greetingProviderImpl));

            // add TemperatureController feature implementation
            var temperatureControllerFeature = ReadFeature("features/TemperatureController.sila.xml");
            _temperatureControllerImpl = new TemperatureControllerImpl
            {
                GreetingProviderImpl = _greetingProviderImpl, SiLAServer = this,
                SiLAFeature = temperatureControllerFeature
            };
            GrpcServer.Services.Add(
                Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController.BindService(
                    _temperatureControllerImpl));

            // set current year as server start year
            _greetingProviderImpl.ServerStartYear = DateTime.Now.Year;
        }


        public void Dispose()
        {
            _temperatureControllerImpl.Dispose();
        }
    }
}