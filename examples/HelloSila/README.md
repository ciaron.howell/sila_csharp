# SiLA C# HelloSiLa
Two sample projects containing a SiLA 2 server as well as a SiLA 2 client demonstrating 
how such an implementation is done. Both projects refer to the [SiLA2Library] (https://gitlab.com/SiLA2/sila_csharp/blob/master/sila_library/src) project, 
containing base classes and utilities, needed to implement SiLA 2 servers or clients.

## HelloSiLa Server
The [HelloSila.ServerApp](https://gitlab.com/SiLA2/sila_csharp/blob/master/examples/HelloSiLa/HelloSiLa.ServerApp) project contains all classes of a SiLA 2 device that implements 
two example Features defined in the `sila_base` repository: a [GreetingProvider](https://gitlab.com/SiLA2/sila_base/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.xml) and 
a [TemperatureController](https://gitlab.com/SiLA2/sila_base/blob/master/feature_definitions/org/silastandard/examples/TemperatureController.xml) feature.

## HelloSiLa Client
The [HelloSiLa.ClientApp](https://gitlab.com/SiLA2/sila_csharp/blob/master/sila_implementations/HelloSiLa/HelloSiLa.ClientApp) project builds a SiLA 2 client using the functionality provided by the HelloSiLa Server.

## Generate the Feature Stubs
In order to generate the stubs run the following commands on a Bash shell 
to generate the protos from features using the `code_generator.jar` and 
stubs from protos using the `protoc` compiler.

```
cd HelloSiLa/HelloSila
generate_stubs.cmd
```

## Build and Run Server and Client
### Build and Run via .NET CLI
The sample applications can just be run from the command line using the `dotnet` CLI.

As a first example lets build and run the C# `HelloSiLa.ServerApp` and `HelloSiLa.ClientApp` applications of the `HelloSiLa` implementation.

Open up a terminal window and run the server
```bash
cd HelloSiLa.ServerApp
dotnet build
dotnet run -- -n <network-interface/ip-range>
```

Now its time to run the Client application, on a separate terminal execute the following set of commands
```bash
cd HelloSiLa.ClientApp
dotnet build
dotnet run
```