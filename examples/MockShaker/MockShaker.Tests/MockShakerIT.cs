namespace Sila2.Examples.MockShaker.Tests
{
    using System;
    using System.Linq;
    using Grpc.Core;
    using Utils;
    using Discovery;
    using ClientApp;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// Fixture class to hold any resources to be shared between tests
    /// e.g. Server and Client
    /// </summary>
    public class ClientServerFixture : IDisposable
    {
        public readonly Client Client;
        public const int ServerPort = 50054;
        private readonly Channel channel;
        private readonly ServerApp.Server server;

        public ClientServerFixture()
        {
            var networkInterface = Networking.FindValidNetworkInterface();
            server = new ServerApp.Server(ServerPort, networkInterface, null);
            server.StartServer();
            channel = SiLADiscovery.GetServers(4000).First(server => server.Config.Name == "Mock Shaker").Channel;
            Client = new Client(channel);
        }

        public void Dispose()
        {
            server.ShutdownServer().Wait();
            channel.ShutdownAsync().Wait();
        }
    }

    /// <summary>
    /// Integration test class to validate functionality of MockShaker server
    /// using client to drive interaction.
    /// </summary>
    public class MockShakerIT : IClassFixture<ClientServerFixture>
    {
        private readonly ITestOutputHelper output;
        private readonly ClientServerFixture fixture;

        public MockShakerIT(ITestOutputHelper output, ClientServerFixture fixture)
        {
            this.output = output;
            this.fixture = fixture;
            Logging.SetupCommonLogging();
            // TODO: add log forwarding to see logs in tests
        }

        [Fact]
        public void TestValidShake()
        {
            fixture.Client.StopShaking();
            fixture.Client.CloseClamp();
            fixture.Client.Shake("PT2S", 3000).Wait(4000);
        }

        [Fact]
        public void TestInvalidShakeWithClampOpen()
        {
            fixture.Client.StopShaking();
            fixture.Client.OpenClamp();
            Assert.ThrowsAsync<RpcException>(async () => { fixture.Client.Shake("PT1S", 3000).Wait(3); });
        }

        [Fact]
        public void TestInvalidDurationParameters()
        {
            fixture.Client.StopShaking();
            fixture.Client.CloseClamp();
            Assert.ThrowsAsync<RpcException>(async () => { fixture.Client.Shake("BLAH", 3000).Wait(); });
        }

        [Fact]
        public void TestClampOpen()
        {
            fixture.Client.OpenClamp();
            Assert.True(fixture.Client.IsClampOpen());
            fixture.Client.CloseClamp();
            Assert.False(fixture.Client.IsClampOpen());
        }
    }
}
