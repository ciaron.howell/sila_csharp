namespace Sila2.Examples.MockShaker.ServerApp
{
    using System;
    using System.Threading;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = NLog.LogManager.GetCurrentClassLogger();
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                // create the shaker server
                Server shaker = null;
                try
                {
                    var networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                    shaker = new Server(options.Port, networkInterface, options.ConfigFile);
                }
                catch (ArgumentNullException)
                {
                    // Create server without without discovery
                    shaker = new Server(options.Port, null, options.ConfigFile);
                }
                // start the server
                shaker.StartServer();

                Log.Info("MockShakerDevice server listening on port " + options.Port);

                Thread.Sleep(3000);
                Console.WriteLine("\nPress enter to stop the server...");
                Console.ReadLine();
                Console.WriteLine("After readline");

                shaker.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}