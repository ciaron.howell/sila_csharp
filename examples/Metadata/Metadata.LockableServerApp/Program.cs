﻿namespace Sila2.Examples.Metadata.LockableServerApp
{
    using System;
    using System.Threading;
    using Common.Logging;
    using Utils;

    public class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Server>();
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                Server server;
                try
                {
                    // create SiLA2 Server
                    var networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                    server = new Server(options.Port, networkInterface, options.ConfigFile);
                }
                catch (ArgumentNullException)
                {
                    // create SiLA2 Server without discovery
                    server = new Server(options.Port, null, options.ConfigFile);
                }
                server.StartServer();
                log.Info("LockableServer listening on port " + options.Port);

                Thread.Sleep(3000);
                Console.WriteLine("\nPress enter to stop the server...");
                Console.ReadLine();

                server.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}
