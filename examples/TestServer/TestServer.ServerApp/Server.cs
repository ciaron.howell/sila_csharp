﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Sila2;
    using Sila2.Server;
 
    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : SiLA2Server
    {
        private readonly DataTypeProviderImpl _dataTypeProviderImpl;
        private static ILog Log = LogManager.GetLogger<Server>();

        public Server(int portNumber, NetworkInterface networkInterface, string configFile)
        : base(new ServerInformation(
                    "DataTypeProvider Server",
                    "Server that implements the DataTypeProvider feature which provides calls using all available SiLA Data Types for their parameters",
                    "www.equicon.de",
                    "1.0"),
                portNumber,
                networkInterface,
                configFile)
        {
            this.ReadFeature("features/DataTypeProvider.sila.xml");
            _dataTypeProviderImpl = new DataTypeProviderImpl();
            this.GrpcServer.Services.Add(Org.Silastandard.Examples.Datatypeprovider.V1.DataTypeProvider.BindService(_dataTypeProviderImpl));
        }
    }
}