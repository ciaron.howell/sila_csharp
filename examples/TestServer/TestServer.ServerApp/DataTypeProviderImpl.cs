﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using Google.Protobuf;
    using Grpc.Core;
    using Org.Silastandard.Examples.Datatypeprovider.V1;
    using SiLAFramework = Org.Silastandard;

    /// <summary>
    /// Implements the functionality of the GreetingsProviderImpl feature.
    /// </summary>
    internal class DataTypeProviderImpl : DataTypeProvider.DataTypeProviderBase
    {
        #region Overrides of DataTypeProviderBase

        public override Task<SetStringValue_Responses> SetStringValue(SetStringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetStringValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received String value: '{request.StringValue.Value}'" } });
        }

        public override Task<Get_StringValue_Responses> Get_StringValue(Get_StringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StringValue_Responses { StringValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" } });
        }

        public override Task<SetIntegerValue_Responses> SetIntegerValue(SetIntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetIntegerValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Integer value: {request.IntegerValue.Value}" } });
        }

        public override Task<Get_IntegerValue_Responses> Get_IntegerValue(Get_IntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_IntegerValue_Responses { IntegerValue = new SiLAFramework.Integer { Value = 5124 } });
        }

        public override Task<SetRealValue_Responses> SetRealValue(SetRealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetRealValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Real value: {request.RealValue.Value}" } });
        }

        public override Task<Get_RealValue_Responses> Get_RealValue(Get_RealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_RealValue_Responses { RealValue = new SiLAFramework.Real { Value = 3.1415926 } });
        }

        public override Task<SetBooleanValue_Responses> SetBooleanValue(SetBooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetBooleanValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Boolean value: {request.BooleanValue.Value}" } });
        }

        public override Task<Get_BooleanValue_Responses> Get_BooleanValue(Get_BooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BooleanValue_Responses { BooleanValue = new SiLAFramework.Boolean { Value = true } });
        }

        public override Task<SetBinaryValue_Responses> SetBinaryValue(SetBinaryValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetBinaryValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Binary value (converted to UTF-8 string): {request.BinaryValue.Value.ToStringUtf8()}" } });
        }

        public override Task<Get_BinaryValue_Responses> Get_BinaryValue(Get_BinaryValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BinaryValue_Responses { BinaryValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Test_String_Value") } });
        }

        public override Task<SetDateValue_Responses> SetDateValue(SetDateValue_Parameters request, ServerCallContext context)
        {
            var dateValue = new DateTime((int)request.DateValue.Year, (int)request.DateValue.Month, (int)request.DateValue.Day);

            return Task.FromResult(new SetDateValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Date value: {dateValue.Date}" } });
        }

        public override Task<Get_DateValue_Responses> Get_DateValue(Get_DateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_DateValue_Responses { DateValue = new SiLAFramework.Date { Year = 2018, Month = 8, Day = 24 } });
        }

        public override Task<SetTimeValue_Responses> SetTimeValue(SetTimeValue_Parameters request, ServerCallContext context)
        {
            var timeValue = new TimeSpan((int)request.TimeValue.Hour, (int)request.TimeValue.Minute, (int)request.TimeValue.Second);

            return Task.FromResult(new SetTimeValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Time value: {timeValue}" } });
        }

        public override Task<Get_TimeValue_Responses> Get_TimeValue(Get_TimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimeValue_Responses { TimeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56 } });
        }

        public override Task<SetTimeStampValue_Responses> SetTimeStampValue(SetTimeStampValue_Parameters request, ServerCallContext context)
        {
            var timestampValue = new DateTime((int)request.TimeStampValue.Year, (int)request.TimeStampValue.Month, (int)request.TimeStampValue.Day, (int)request.TimeStampValue.Hour, (int)request.TimeStampValue.Minute, (int)request.TimeStampValue.Second);

            return Task.FromResult(new SetTimeStampValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received TimeStamp value: {timestampValue}" } });
        }

        public override Task<Get_TimeStampValue_Responses> Get_TimeStampValue(Get_TimeStampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimeStampValue_Responses { TimeStampValue = new SiLAFramework.Timestamp { Year = 2018, Month = 8, Day = 24, Hour = 12, Minute = 34, Second = 56 } });
        }

        public override Task<SetStringList_Responses> SetStringList(SetStringList_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received String values: ");
            foreach (var stringValue in request.StringList)
            {
                sb.Append($"'{stringValue}', ");
            }

            return Task.FromResult(new SetStringList_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.Remove(sb.Length - 2, 2).ToString() } });
        }

        public override Task<Get_StringList_Responses> Get_StringList(Get_StringList_Parameters request, ServerCallContext context)
        {
            var response = new Get_StringList_Responses
            {
                StringList =
                {
                    new SiLAFramework.String { Value = "SiLA 2"},
                    new SiLAFramework.String { Value = "is"},
                    new SiLAFramework.String { Value = "great"}
                }
            };

            return Task.FromResult(response);
        }

        public override Task<SetIntegerList_Responses> SetIntegerList(SetIntegerList_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received String values: ");
            foreach (var intValue in request.IntegerList)
            {
                sb.Append($"{intValue}, ");
            }

            return Task.FromResult(new SetIntegerList_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.Remove(sb.Length - 2, 2).ToString() } });
        }

        public override Task<Get_IntegerList_Responses> Get_IntegerList(Get_IntegerList_Parameters request, ServerCallContext context)
        {
            var response = new Get_IntegerList_Responses
            {
                IntegerList =
                {
                    new SiLAFramework.Integer{ Value = 1},
                    new SiLAFramework.Integer { Value = 2},
                    new SiLAFramework.Integer { Value = 3}
                }
            };

            return Task.FromResult(response);
        }

        public override Task<SetSeveralValues_Responses> SetSeveralValues(SetSeveralValues_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received values: ");

            sb.Append($"\n\tString value: '{request.StringValue.Value}'");
            sb.Append($"\n\tInteger value: {request.IntegerValue.Value}");
            sb.Append($"\n\tReal value: {request.RealValue.Value}");
            sb.Append($"\n\tBoolean value: {request.BooleanValue.Value}");
            var dateValue = new DateTime((int)request.DateValue.Year, (int)request.DateValue.Month, (int)request.DateValue.Day);
            sb.Append($"\n\tDate value: {dateValue.Date}");
            var timeValue = new TimeSpan((int)request.TimeValue.Hour, (int)request.TimeValue.Minute, (int)request.TimeValue.Second);
            sb.Append($"\n\tTime value: {timeValue}");
            var timestampValue = new DateTime((int)request.TimeStampValue.Year, (int)request.TimeStampValue.Month, (int)request.TimeStampValue.Day, (int)request.TimeStampValue.Hour, (int)request.TimeStampValue.Minute, (int)request.TimeStampValue.Second);
            sb.Append($"\n\tTimeStamp value: {timestampValue}");

            return Task.FromResult(new SetSeveralValues_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.ToString() } });
        }

        public override Task<GetSeveralValues_Responses> GetSeveralValues(GetSeveralValues_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new GetSeveralValues_Responses
            {
                StringValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" },
                IntegerValue = new SiLAFramework.Integer { Value = 5124 },
                RealValue = new SiLAFramework.Real { Value = 3.1415926 },
                BooleanValue = new SiLAFramework.Boolean { Value = true },
                DateValue = new SiLAFramework.Date { Year = 2018, Month = 8, Day = 24 },
                TimeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56 },
                TimeStampValue = new SiLAFramework.Timestamp { Year = 2018, Month = 8, Day = 24, Hour = 12, Minute = 34, Second = 56 },
            });
        }

        #endregion
    }
}