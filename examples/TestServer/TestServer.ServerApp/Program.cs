﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System;
    using System.Threading;
    using Common.Logging;
    using Utils;

    public class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = LogManager.GetLogger<Program>();
            try
            {
                // Parse cmd line args
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                // create SiLA2 Server
                // start the gRPC Server
                Server server;
                try
                {
                    var networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                    server = new Server(options.Port, networkInterface, options.ConfigFile);
                }
                catch (ArgumentNullException)
                {
                    // Running w/o discovery
                    server = new Server(options.Port, null, options.ConfigFile);
                }
                server.StartServer();
                Log.Info("DataTypeProvider Server listening on port " + options.Port);

                Thread.Sleep(3000);
                Log.Info("\nPress enter to stop the server...");
                Console.ReadLine();

                server.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                Log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}