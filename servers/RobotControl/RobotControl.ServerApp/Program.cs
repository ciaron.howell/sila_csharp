using System;
using System.Net.NetworkInformation;
using System.Threading;
using Common.Logging;
using RobotControl.Sila;
using Sila2;
using Sila2.Utils;

namespace RobotControl
{
    internal class ServerApp
    {
        private IRobotController _robotController;
        private IRobotModelsManager _robotModelsManager;

        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();

            var log = LogManager.GetLogger<Server>();
            var serverApp = new ServerApp();
            var factory = new RobotFactory();
            factory.Create(out serverApp._robotController, out serverApp._robotModelsManager);
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                NetworkInterface networkInterface;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // create SiLA2 Server without discovery
                    networkInterface = null;
                }

                var serverInfo = new ServerInformation(
                    "Robot Control Server",
                    "Server that implements the features for robot control including calibration, teaching, localization and status",
                    "www.unitelabs.ch",
                    "1.0");

                var server = new Server(options.Port, networkInterface, options.ConfigFile, serverApp._robotController,
                    serverApp._robotModelsManager, serverInfo);
                
                server.SimulationController.ModeChanged += (sender, eventArgs) =>
                {
                    factory.Create(out serverApp._robotController, out serverApp._robotModelsManager,
                        eventArgs.SimulationMode);
                };

                log.Info("Robot Control Server listening on port " + options.Port);

                Thread.Sleep(3000);
                Console.WriteLine("\nPress enter to stop the server...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}