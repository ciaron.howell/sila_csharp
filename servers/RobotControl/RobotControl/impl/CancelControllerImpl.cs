using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using RobotControl;
using Sila2.Org.Silastandard.Core.Commands.Cancelcontroller.V1;
using Sila2.Utils;

namespace RobotControl.Impl
{
    public class CancelControllerImpl : CancelController.CancelControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger<RobotControllerImpl>();
        private readonly IRobotController _robotController;

        public CancelControllerImpl(IRobotController robotController)
        {
            _robotController = robotController;
        }

        public override Task<CancelCommand_Responses> CancelCommand(CancelCommand_Parameters request, ServerCallContext context)
        {
            Cancel();
            return Task.FromResult(new CancelCommand_Responses());
        }

        public override async Task<CancelAll_Responses> CancelAll(CancelAll_Parameters request, ServerCallContext context)
        {
            Cancel();
            return new CancelAll_Responses();
        }

        private void Cancel()
        {
            try
            {
                _robotController.Cancel().Wait();
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
        }
    }
}