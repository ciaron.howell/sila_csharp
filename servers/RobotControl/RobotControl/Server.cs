﻿using System;
using System.Net.NetworkInformation;
using Common.Logging;
using RobotControl.Impl;
using Sila2;
using Sila2.Server;

namespace RobotControl.Sila
{
    using Sila2.Ch.Unitelabs.Robot.Robotteachingservice.V1;
    using Sila2.Ch.Unitelabs.Robot.Robotcontroller.V1;
    using Sila2.Ch.Unitelabs.Robot.Devicecalibrationservice.V1;
    using Sila2.Ch.Unitelabs.Robot.Batterycontroller.V1;
    using Sila2.Ch.Unitelabs.Robot.Gripcontroller.V1;
    using Sila2.Ch.Unitelabs.Core.Initializationcontroller.V1;
    using Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1;
    using Sila2.Org.Silastandard.Core.Commands.Cancelcontroller.V1;
    using Grpc.Core;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a set of features for robot interactions
    /// </summary>
    public class Server : IDisposable
    {
        private static readonly ILog Log = LogManager.GetLogger<Server>();
        private readonly SiLA2Server _siLa2Server;
        public readonly SimulationControllerImpl SimulationController;

        public Server(int portNumber, NetworkInterface networkInterface, string configFile,
            IRobotController robotController, IRobotModelsManager robotModelsManager, ServerInformation serverInfo)
        {
            _siLa2Server = new SiLA2Server(serverInfo, portNumber, networkInterface, configFile);
            SimulationController = new SimulationControllerImpl();

            _siLa2Server.AddFeature("features/RobotController.sila.xml",
                RobotTeachingService.BindService(new RobotTeachingServiceImpl(robotController, robotModelsManager)));
            _siLa2Server.AddFeature("features/DeviceCalibrationService.sila.xml",
                RobotController.BindService(new RobotControllerImpl(robotController)));
            _siLa2Server.AddFeature("features/InitializationController.sila.xml",
                InitializationController.BindService(new InitializationControllerImpl(robotController)));
            _siLa2Server.AddFeature("features/RobotTeachingService.sila.xml",
                DeviceCalibrationService.BindService(new DeviceCalibrationServiceImpl(robotModelsManager)));
            _siLa2Server.AddFeature("features/SimulationController.sila.xml",
                Sila2.Org.Silastandard.Core.Simulationcontroller.V1.SimulationController.BindService(
                    SimulationController));
            _siLa2Server.AddFeature("features/GripController.sila.xml",
                GripController.BindService(new GripControllerImpl(robotController)));
            _siLa2Server.AddFeature("features/CancelController.sila.xml",
                CancelController.BindService(new CancelControllerImpl(robotController)));
            _siLa2Server.AddFeature("features/BatteryController.sila.xml",
                BatteryController.BindService(new BatteryControllerImpl(robotController)));
            _siLa2Server.AddFeature("features/PlateCalibrationService.sila.xml",
                PlateCalibrationService.BindService(new PlateCalibrationServiceImpl(robotModelsManager)));
            
            _siLa2Server.StartServer();
        }

        /// <summary>
        /// Adds any additional feature definitions which the server should expose 
        /// </summary>
        /// <param name="featureDefinitionFile">path to resource</param>
        /// <param name="bindableService">Bindable gRPC feature implementation</param>
        public void AddFeature(string featureDefinitionFile, ServerServiceDefinition bindableService)
        {
            _siLa2Server.AddFeature(featureDefinitionFile, bindableService);
        }

        public void Dispose()
        {
            _siLa2Server.ShutdownServer().Wait();
        }
    }
}