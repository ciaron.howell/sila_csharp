# Robot Control SiLA Server
This is a generic robot control SiLA 2 server which aims to provide a standardises interface that can be used with any robot. Specific robot implementations can be provided by implementing the provided C# interfaces.

## Project Architecture

- `RobotControl`: All SiLA resources and implementations to the gRPC generated base classes live here, including the server implementation
- `RobotControl.Handler`: All interfaces used to interact with the physical instrument shall be defined here, only abstract interfaces and simulation mode implementations should exist here.
- `RobotControl.Server`: Server application which instantiates the Robot Control server app, only app in the project

## Generating Protos and Stubs

Quick little cmd to automatically generate all stubs in one blast (in bash):

```bash
find RobotControl.Sila/features/ -name '*.sila.xml' -exec ../../tools/generate_proto_and_stub.sh {} RobotControl.Sila/generated_code/protos RobotControl.Sila/generated_code/stubs \;
```

Generate Standard features

```bash
../../tools/generate_proto_and_stub.sh ../../sila_base/feature_definitions/org/silastandard/core/SimulationController.sila.xml RobotControl.Sila/generated_code/protos RobotControl.Sila/generated_code/stubs
../../tools/generate_proto_and_stub.sh ../../sila_base/feature_definitions/org/silastandard/core/CancelController.sila.xml RobotControl.Sila/generated_code/protos RobotControl.Sila/generated_code/stubs
```
