using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Logging;
using Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1;
using Sila2.Ch.Unitelabs.Robot.Devicecalibrationservice.V1;
using DataType_Site = Sila2.Ch.Unitelabs.Robot.Robotcontroller.V1.DataType_Site;

namespace RobotControl.Implementations
{
    public class RobotSimulator : IRobotController, IRobotModelsManager
    {
        private readonly ILog _log = LogManager.GetLogger<RobotSimulator>();
        private static readonly int TaskDelay = 3000;
        private bool _teachMode;
        public double BatteryPercentage { get; set; }
        public int BatteryEstimatedTime { get; set; }
        public bool TeachMode { get => _teachMode; set => FreeMode(value); }
        public Dictionary<string, DataType_PlateDefinition.Types.PlateDefinition_Struct> PlateTypes => GetPlateTypes();
        public List<string> TaughtPositions { get => new List<string> {"locationA", "locationB"}; }
        
        private Task FreeMode(bool turnOn)
        {
            _teachMode = turnOn;
            _log.Debug($"FreeMode: {_teachMode}");
            return Task.CompletedTask;
        }

        public async Task Initialize()
        {
            _log.Info("Initializing.... ");
            await Task.Delay(TaskDelay);
        }

        public bool IsOccupied(DataType_Site.Types.Site_Struct site)
        {
            _log.Info($"Checking if {site} is occupied");
            Task.Delay(TaskDelay).Wait();
            return true;
        }

        public Task MoveToPosition(string positionName)
        {
            _log.Debug("MoveToSafe");
            return Task.CompletedTask;
        }

        public Task MoveToSite(DataType_Site.Types.Site_Struct site)
        {
            return Task.Run(() =>
            {
                _log.Info($"MoveTo {site.Device.Value} at {site.SiteIndex.Value}");
                Task.Delay(TaskDelay);
            });
        }

        public Task Retract()
        {
            _log.Info("Retract");
            Task.Delay(TaskDelay);
            return Task.CompletedTask;
        }

        public Task ApproachTo(DataType_Site.Types.Site_Struct site)
        {
            return Task.Run(() =>
            {
                _log.Info($"ApproachTo {site.Device.Value} at {site.SiteIndex.Value}");
                Task.Delay(TaskDelay);
            });
        }

        public Task MovePlate(DataType_Site.Types.Site_Struct srcSite, DataType_Site.Types.Site_Struct dstSite, string plateType)
        {
            return Task.Run(() =>
            {
                _log.Info($"MovePlate {srcSite.Device.Value}  at{srcSite.SiteIndex.Value} to {dstSite.Device.Value} at {dstSite.SiteIndex.Value}");
                Task.Delay(TaskDelay);
            });
        }
        
        public Task PickPlate(DataType_Site.Types.Site_Struct siteSite, string plateType)
        {
            return Task.Run(() =>
            {
                _log.Info("PickPlate");
                Task.Delay(TaskDelay);
            });
        }

        public Task PlacePlate(DataType_Site.Types.Site_Struct siteSite,string plateType)
        {
            return Task.Run(() =>
            {
                _log.Info("PlacePlate");
                Task.Delay(TaskDelay);
                return Task.CompletedTask;
            });
        }

        public Task Charge()
        {
            return Task.Run(() =>
            {
                _log.Info("Charge");
                Task.Delay(TaskDelay);
            });
        }

        public Task Grip(bool close)
        {
            var gripState = close ? "Closed" : "Open";
            _log.Info($"Gripper is {gripState}");
            return Task.CompletedTask;
        }

        public Task Cancel()
        {
            _log.Info($"Cancelled all running tasks!");
            return Task.CompletedTask;
        }

        public Task GripCalibrationPlate(Orientation plateOrientation)
        {
            _log.Info($"Grip calibration plate in {plateOrientation} orientation");
            return Task.CompletedTask;
        }
        public void SetDevice(string deviceName, int numSites, int height, int approach)
        {
            _log.Info($"CreateDevice: {deviceName}{numSites}{height}{approach}");
        }

        public void DeleteDevice(string deviceName)
        {
            _log.Debug("Delete Device");
        }

        public void DeleteDevice(string deviceName, int numSites, int height, int approach)
        {
            _log.Info($"DeleteDevice: {deviceName}{numSites}{height}{approach}");
        }
        
        public List<DataType_DeviceProperties.Types.DeviceProperties_Struct> Devices()
        {
            _log.Info("Devices");
            return new List<DataType_DeviceProperties.Types.DeviceProperties_Struct> {new DataType_DeviceProperties.Types.DeviceProperties_Struct()};
        }

        public void CalibrateMarkers(string deviceName)
        {
            _log.Info("CalibrateMarker");
        }

        public void CalibrateSite(string deviceName, int siteIndex, bool delidSite = false)
        {
            throw new NotImplementedException();
        }

        public void CalibrateSite(string deviceName, int siteIndex)
        {
            _log.Info("CalibrateAssociatedSite");
        }
        public void TeachDevice(string deviceName)
        {
            _log.Info($"Teach: {deviceName}");
        }

        public void TeachPosition(string positionValue)
        {
            throw new NotImplementedException();
        }

        public void DeleteLocation(string deviceName)
        {
            _log.Info($"DeleteLocation: {deviceName}");
        }

        public void CreatePlateType(DataType_PlateDefinition.Types.PlateDefinition_Struct plateType)
        {
            _log.Info($"CreatePlateType: {plateType}");
        }

        public void DeletePlateType(string plateType)
        {
            _log.Info($"DeletePlateType: {plateType}");
        }

        private Dictionary<string, DataType_PlateDefinition.Types.PlateDefinition_Struct> GetPlateTypes()
        {
            _log.Info("PlateTypes");
            var plateType = new DataType_PlateDefinition.Types.PlateDefinition_Struct
            {
                Name = new Sila2.Org.Silastandard.String {Value = "TypeA"},
                Height = new Sila2.Org.Silastandard.Real {Value = 0.3},
                GripHeight = new Sila2.Org.Silastandard.Real {Value = 10},
                LidHeight = new Sila2.Org.Silastandard.Real {Value = 20}
            };
            var list = new Dictionary<string, DataType_PlateDefinition.Types.PlateDefinition_Struct>();
            list.Add(plateType.Name.Value, plateType);
            plateType.Name = new Sila2.Org.Silastandard.String {Value = "TypeB"};
            list.Add(plateType.Name.Value, plateType);
            return list;
        }
    }
}