# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and starting with version number 1.0.0 this project will adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The version numbers in this changelog refer to the `Sila2` core library.

## [vNext]

### Incompatible Changes
- Update the README with list of supported instruments

### Compatible Changes
- ...

## [0.2.4]

### Compatible Changes
- !99 Fix error when no more plates are available with Biostack 2  

## [0.2.3]

### Compatible Changes
- !98 Fix double homing issue
- !98 Update the stacker controller so it can only run one command at a time
- !98 Fix error message when the stacker is disabled
- !98 Fix Biotek 2 to pickup plates after the first one

## [0.2.2]

### Compatible Changes
- !97 Allow Biotek server to start even if instruments are not plugged in or available 

## [0.2.1]

### Compatible Changes
- !94 Biotek Server to support Biostack 2 stack and feed plates

## [0.2.0] - (introduce basic functionality with Biostack 3 and 4)

## Incompatible Changes
- !92 Biotek Server to support Biostack 3 and 4 to stack and feed plates

## [0.1.1] - (introduction of changelog, fixes for integration with BioStack)

### Compatible Changes
- Protocol validation is done on first run of program and retreives settings from instrument
- Load protocol from flash only once, if the protocol is repeated

## [0.1.0] - (first release or PoC)
- first release
- basic functionality to load program list and execute Biotek programs