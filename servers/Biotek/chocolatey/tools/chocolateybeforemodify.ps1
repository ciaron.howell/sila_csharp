﻿# Back up the server configuration file if it exists
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$configFile = Join-Path $toolsDir 'app_config.json'
$appDataDir = Join-Path $env:LOCALAPPDATA 'sila'
$appDataDir = Join-Path $appDataDir 'biotek'

Write-Warning "Will back up files to $appDataDir"
 
if ([System.IO.Directory]::Exists($configFile)) {

    Copy-Item $configFile -Destination $appDataDir -Recurse -Force
    Write-Warning "To prevent accidental data loss the server configuration has been backed up $appDataDir"
} else {
    Write-Warning "There was no '$configFile' found to backup"
}

# TODO: back up the sila configuration