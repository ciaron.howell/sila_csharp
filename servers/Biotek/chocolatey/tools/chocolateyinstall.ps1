﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

# TODO: use some property to get version number
$fileLocation = Join-Path $toolsDir 'Biotek.ServerApp.0.2.4.win-x86.zip'
Get-ChocolateyUnzip $fileLocation $toolsDir

$savedConfigFile = Join-Path $env:LOCALAPPDATA 'sila'
$savedConfigFile = Join-Path $savedConfigFile 'biotek'
$savedConfigFile = Join-Path $savedConfigFile 'app_config.json'

if ([System.IO.Directory]::Exists($savedConfigFile)) {
    Copy-Item $savedConfigFile -Destination $toolsDir
    Write-Warning "Restored app_config.json from $savedConfigFile"
} else {
    Write-Warning "There was no $savedConfigFile to restore" 
}