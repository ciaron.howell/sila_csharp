using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Biotek.Handler.Stacker;
using Biotek.Handler.Washer;
using Common.Logging;
using Grpc.Core;
using Sila2;
using Sila2.Ch.Unitelabs.Core.Programcontroller.V1;
using Sila2.Org.Silastandard;
using Sila2.Utils;

namespace Biotek.Sila
{
    class ProgramControllerImpl : ProgramController.ProgramControllerBase
    {
        private static readonly ILog Logger = LogManager.GetLogger<ProgramControllerImpl>();
        private readonly object _tokenLock = new object();
        private readonly IWasherController _washerController;
        private readonly IStackerController _stackerController;
        private readonly ObservableCommandManager<Run_Parameters, Run_Responses> _runCommandManager =
                new ObservableCommandManager<Run_Parameters, Run_Responses>(TimeSpan.FromHours(1));
        private CancellationTokenSource _cancellationToken = null;
        

        public ProgramControllerImpl(IWasherController washerController, IStackerController stackerController)
        {
            _washerController = washerController;
            _stackerController = stackerController;
        }

        public override async Task<CommandConfirmation> Run(Run_Parameters request, ServerCallContext context)
        {
            TryCreateTokenWithLock();
            var addCommand = await _runCommandManager.AddCommand(request, RunProtocol);
            return addCommand.Confirmation; 
        }

        public override async Task Run_Info(CommandExecutionUUID request, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _runCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                Logger.Warn(e);
                throw;
            }
        }

        public override Task<Run_Responses> Run_Result(CommandExecutionUUID request, ServerCallContext context)
        {
            return Task.FromResult(_runCommandManager.GetCommand(request).Result());
        }

        public override Task<Stop_Responses> Stop(Stop_Parameters request, ServerCallContext context)
        {
            try
            {
                _cancellationToken?.Cancel();
            }
            catch (Exception e)
            {
                if (e is OperationCanceledException)
                {
                    Logger.Info("Run task cancelled");
                }
                else
                {
                    Logger.Warn(e);
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError("StopError", e.Message));
                }
            }
            return Task.FromResult(new Stop_Responses());
        }

        public override Task<Get_ListPrograms_Responses> Get_ListPrograms(Get_ListPrograms_Parameters request, ServerCallContext context)
        {
            var response = new Get_ListPrograms_Responses();
            try
            {
                foreach (var protocol in _washerController.GetProtocols())
                {
                    response.ListPrograms.Add(new Sila2.Org.Silastandard.String {Value = protocol});
                }                    
            }
            catch (Exception e)
            {
                Logger.Warn(e);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError("ListProgramsError", e.Message));
            }
            return Task.FromResult(response);
        }
        private Run_Responses RunProtocol(IProgress<ExecutionInfo> progress, Run_Parameters param, CancellationToken cancellationToken)
        {
            var programName = param.ProgramName.Value;
            try
            {
                progress.Report(new ExecutionInfo
                {
                    CommandStatus = ExecutionInfo.Types.CommandStatus.Running
                    // todo is there a way to estimate the time the run will take?
                });
                if (!_washerController.GetProtocols().Contains(programName))
                {
                    throw new WasherException($"Protocol {programName} does not exist!");
                }

                if (_stackerController.StackMode)
                {
                    try
                    {
                        _stackerController.Initialize();
                        while (_stackerController.PlateState != PlateState.NoMorePlate)
                        {
                            _stackerController.LoadPlate();
                            if (_stackerController.PlateState != PlateState.NoMorePlate)
                            {
                                var runTask = _washerController.RunProtocol(programName, _cancellationToken.Token);
                                _stackerController.PrepareNext();
                                runTask.Wait(cancellationToken);
                                Task.Delay(1500).Wait(cancellationToken); // TODO can we improve this so we dont have to add a delay?
                            }
                        }
                    }
                    catch (StackerInstrumentException e)
                    {
                        if (e.Code != BiostackError.NO_PLATE_IN_GRIPPER_1)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        if (_stackerController.RestackMode)
                        {
                            _stackerController.Restack();
                        }
                    }
                }
                else
                {
                    var runTask = _washerController.RunProtocol(programName, _cancellationToken.Token);
                    runTask.Wait(cancellationToken);
                }

                Logger.Info("Observable command protocol finished successfully");
                return new Run_Responses();
            }
            catch (Exception e)
            {
                if (e is OperationCanceledException)
                {
                    Logger.Info("Run task cancelled");
                    ErrorHandling.RaiseSiLAError(
                        ErrorHandling.CreateDefinedExecutionError("InvalidState", "Run was stopped by user")
                    );
                }
                Logger.Warn("Observable command protocol run failed ", e);
                ErrorHandling.RaiseSiLAError(e is WasherException ? 
                    ErrorHandling.CreateDefinedExecutionError("InvalidState", ErrorHandling.FlattenExceptionsMessageToString(e)) :
                    ErrorHandling.CreateDefinedExecutionError("RunError", ErrorHandling.FlattenExceptionsMessageToString(e)));
                throw; // Shouldn't be reachable, just to make the IDE Happy
            }
            finally
            {
                ResetTokenWithLock();        
            }
        }
        
        private void TryCreateTokenWithLock()
        {
            lock (_tokenLock)
            {
                if (_cancellationToken != null)
                {
                    Logger.Warn("Another protocol is already running!");
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError("InvalidState", "Another program is already running!"));
                    return;
                }
                _cancellationToken = new CancellationTokenSource();
            }
        }
        
        private void ResetTokenWithLock()
        {
            lock (_tokenLock)
            {
                _cancellationToken = null;
            }        
        }
    }
}