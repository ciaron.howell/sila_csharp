namespace Biotek.Handler.Stacker
{
    public interface IStackerController
    {
        bool RestackMode { set; get; }
        bool StackMode { set; get; }
        bool TeachMode { set; get; }
        PlateState PlateState { get; }
        InstrumentType Instrument { set; get; }
        InstrumentInterface InstrumentInterface { set; get; }

        void JogUp(int steps);
        void JogDown(int steps);
        void TeachThis();
        void Initialize();
        void LoadPlate();
        void PrepareNext();
        void Restack();
    }
}