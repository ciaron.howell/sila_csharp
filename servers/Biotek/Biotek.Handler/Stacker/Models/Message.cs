using System;
using System.Linq;
using Common.Logging;

namespace Biotek.Handler.Stacker
{
    internal class Message
    {
        private const int MAX_BODY_BYTES = 60;
        
        private static readonly ILog Log = LogManager.GetLogger<Message>();

        public readonly Header Header;
        public readonly char[] Body = Enumerable.Repeat('\0', MAX_BODY_BYTES).ToArray();

        internal Message(Command command)
        {
            Header = new Header(command) {MessageBodySize = 0};
            WriteChecksum();
        }

        internal Message(Command command, char[] msgBody)
        {
            Header = new Header(command);
            Body = msgBody;
            Header.MessageBodySize = Convert.ToUInt16(Body?.Length ?? 0);
            Log.Debug($"Creating message with body: {msgBody}");
            WriteChecksum();
        }

        internal Message(Header header)
        {
            Header = header;
            Body = Enumerable.Repeat('\0', Header.MessageBodySize).ToArray();
        }

        public override string ToString()
        {
            var bodyHex = BitConverter.ToString(Body.ToList().Select(Convert.ToByte).ToArray());
            return $"{Header}\nBody: {bodyHex}";
        }

        /// <summary> ComputeChecksum method
        /// Computes the checksum for this message
        /// Assumes that the checksum is the last item in the header
        /// </summary>
        private ushort ComputeChecksum()
        {
            var runningSum = SumHeaderWithoutChecksum();

            for (var i = 0; i < Header.MessageBodySize; i++)
            {
                unchecked // because of potential underflow
                {
                    runningSum += Body[i];                    
                }
            }

            //unRunningSum &= 0x0000FFFF;
            var unTwosComplementSum = Convert.ToUInt16(0xFFFF);
            unTwosComplementSum -= runningSum;
            unTwosComplementSum += 1;

            return unTwosComplementSum;
        }

        public void Validate()
        {
            ValidateChecksum();

            // Evaluate the return code(s)
            var nIndex = 0;
            var statusCode = CharArrayToUInt16(Body[nIndex++], Body[nIndex++]);
            if (statusCode != BiostackError.INVALID_REQUEST.Value)
            {
                return;
            }
            var errorCode = CharArrayToUInt16(Body[nIndex++], Body[nIndex++]);
            throw BiostackError.Create(errorCode);
        }

        private void ValidateChecksum()
        {
            var unCalculatedChecksum = ComputeChecksum();
            if (Math.Abs(unCalculatedChecksum - Header.Checksum) > 2)
            {
                throw new StackerException("Checksum error!");
            }
        }

        private void WriteChecksum()
        {
            Header.Checksum = ComputeChecksum();
        }

        private ushort SumHeaderWithoutChecksum()
        {
            ushort runningSum = 0;
            const int numBytesInChecksum = sizeof(ushort);

            for (var i = 0; i < Header.HEADER_SIZE - numBytesInChecksum; i++)
            {
                unchecked // because of potential underflow
                {
                    runningSum += Header.Data[i];
                }
            }

            return runningSum;
        }

        public static ushort CharArrayToUInt16(char cLoByte, char cHiByte)
        {
            var unInt16 = Convert.ToUInt16(cLoByte);
            unInt16 += Convert.ToUInt16(cHiByte * 256);
            return unInt16;
        }

        public static uint CharArrayToUInt(char cLoByte1, char cHiByte1, char cLoByte2, char cHiByte2)
        {
            uint unInt32 = 0;
            unInt32 += Convert.ToUInt32(cLoByte1);
            unInt32 += Convert.ToUInt32((uint) cHiByte1 * 256);
            unInt32 += Convert.ToUInt32((uint) cLoByte2 * 256 * 256);
            unInt32 += Convert.ToUInt32((uint) cHiByte2 * 256 * 256 * 256);
            return unInt32;
        }
    }
}