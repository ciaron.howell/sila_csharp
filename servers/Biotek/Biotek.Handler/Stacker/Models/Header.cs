using System;
using System.Linq;
using Common.Logging;

namespace Biotek.Handler.Stacker
{
    internal class Header
    {
        internal const int HEADER_SIZE = 11;
        private const byte DESTINATION_PC = 1;
        private const byte DESTINATION_INSTRUMENT = 2;
        private const byte MSG_CLASS_REQ_MSG = 1;

        private static readonly ILog Log = LogManager.GetLogger<Header>();
        private byte _structure; // The source is usually the PC
        private byte _destination; // Good to have in case there are multiple instruments that could receive a message
        private short _object; // The message object, either common or instrument-specific
        private byte _messageClass; // Only byREQ_MSG is used

        private ushort _messageId; // This potential field is used in Immucor's G3 instrument to 

        // uniquely identify which message went out, in case multiple 
        // messages of the same object type are pending responses

        internal Header(Command command)
        {
            InitializeHeader();
            _object = (short) command;
            Log.Debug($"Creating header with command: {command} ({_object})");
        }
        
        private Header() {}

        public ushort MessageBodySize { get; set; }
        public ushort Checksum { get; set; }
        internal char[] Data
        {
            get => Serialize();
            set => Deserialize(value);
        }

        public static Header Deserialize(char[] buffer)
        {
            return new Header
            {
                _structure = Convert.ToByte(buffer[0]),
                _destination = Convert.ToByte(buffer[1]),
                // The next two bytes are in reverse order for Int16
                _object = Convert.ToInt16((buffer[3] << 8) + buffer[2]),
                _messageClass = Convert.ToByte(buffer[4]),
                // The next two bytes are in reverse order for UInt16
                _messageId = Convert.ToUInt16((buffer[6] << 8) + buffer[5]),
                // The next two bytes are in reverse order for UInt16
                MessageBodySize = Convert.ToUInt16((buffer[8] << 8) + buffer[7]),
                // The next two bytes are in reverse order for UInt16
                Checksum = Convert.ToUInt16((buffer[10] << 8) + buffer[9])
            };
        }

        public override string ToString()
        {
            return "Header: " + BitConverter.ToString(Data.ToList().Select(Convert.ToByte).ToArray());
        }

        private void InitializeHeader()
        {
            _structure = 1;
            _destination = DESTINATION_INSTRUMENT;
            _messageClass = MSG_CLASS_REQ_MSG;
        }

        /**
         * Convert the header data to serialized buffer to send via Serial
         */
        private char[] Serialize()
        {
            var cArray = new char[HEADER_SIZE];

            int nIndex = 0;
            cArray[nIndex++] = Convert.ToChar(_structure);
            cArray[nIndex++] = Convert.ToChar(_destination);
            cArray[nIndex++] = Convert.ToChar(_object & 0x00FF); // Lo byte
            cArray[nIndex++] = Convert.ToChar(_object >> 8); // Hi byte
            cArray[nIndex++] = Convert.ToChar(_messageClass);

            cArray[nIndex++] = Convert.ToChar(_messageId & 0x00FF); // Lo byte
            cArray[nIndex++] = Convert.ToChar(_messageId >> 8); // Hi byte

            cArray[nIndex++] = Convert.ToChar(MessageBodySize & 0x00FF); // Lo byte
            cArray[nIndex++] = Convert.ToChar(MessageBodySize >> 8); // Hi byte
            cArray[nIndex++] = Convert.ToChar(Checksum & 0x00FF); // Lo byte
            cArray[nIndex++] = Convert.ToChar(Checksum >> 8); // Hi byte

            return cArray;
        }
    }
}