using System;
using System.Collections.Generic;

namespace Biotek.Handler.Washer
{
    public class InstrumentType
    {
        private InstrumentType(string value) { Value = value; }

        public string Value { get; }

        public static readonly InstrumentType EL406 = new InstrumentType("EL406");
        public static readonly InstrumentType ELx405 = new InstrumentType("ELx405");
        public static readonly InstrumentType MicroFlo = new InstrumentType("MicroFlo Select");
        public static readonly InstrumentType MultiFlo = new InstrumentType("MultiFlo");
        public static readonly InstrumentType _405TS = new InstrumentType("405 TS/LS");
        public static readonly InstrumentType MultiFloFX = new InstrumentType("MultiFloFX");
        public static readonly InstrumentType _50TS = new InstrumentType("50 TS");

        public static InstrumentType FromString(string instrumentType)
        {
            if (!Types.Contains(instrumentType))
            {
                throw new ArgumentException($"{instrumentType} is not a valid InstrumentType !");
            }
            return new InstrumentType(instrumentType);
        }
        
        public static readonly SortedSet<string> Types = new SortedSet<string>
        {
            EL406.Value, ELx405.Value, MicroFlo.Value, MultiFlo.Value, _405TS.Value, MultiFloFX.Value, _50TS.Value
        };
    }
}