using System;
using System.IO;
using System.Net.NetworkInformation;
using Biotek.Handler.Stacker;
using Biotek.Handler.Washer;
using Biotek.Sila;
using NLog;
using NLog.Targets;
using Sila2.Utils;
using InstrumentType = Biotek.Handler.Stacker.InstrumentType;

namespace Biotek.ServerApp
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetCurrentClassLogger();
            try
            {
                var options = CmdLineArgs.Parse<BiotekCmdLineArgs>(args);

                if (!Handler.Washer.InstrumentType.Types.Contains(options.InstrumentType))
                {
                    throw new ArgumentException($"This driver is not compatible with the instrument {options.InstrumentType}, only the following instruments are supported: {string.Join(", ", Handler.Washer.InstrumentType.Types)}");
                }

                if (options.LogDir != null)
                {
                    var target = (FileTarget)LogManager.Configuration.FindTargetByName("LogFileTarget");
                    target.FileName = Path.Combine(options.LogDir, "server.log");
                    LogManager.ReconfigExistingLoggers();
                }

                Server server;
                WasherController washerController;
                if (options.DllPath != null)
                {
                    washerController = new WasherController(Handler.Washer.InstrumentType.FromString(options.InstrumentType), options.WasherComPort, options.DllPath);
                }
                else
                {
                    washerController = new WasherController(Handler.Washer.InstrumentType.FromString(options.InstrumentType), options.WasherComPort);
                }

                IStackerController stackerController;
                if (options.StackerComPort != null)
                {
                    stackerController = new SynchronousStackerController(new StackerController(options.StackerComPort));
                    stackerController.StackMode = true;
                }
                else
                {
                    stackerController = new DisabledStackerController();
                }

                NetworkInterface networkInterface;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // create SiLA2 Server without discovery
                    networkInterface = null;
                }
                server = new Server(options.Port, networkInterface, options.ConfigFile, washerController, stackerController);

                server.StartServer();
                log.Info("Example Server listening on port " + options.Port);

                log.Info("Press enter to stop the server...");
                Console.ReadLine();

                var shutdownServerTask = server.ShutdownServer();
                washerController.Dispose();
                shutdownServerTask.Wait(TimeSpan.FromSeconds(5));
            }
            catch (Exception e)
            {
                log.Error( e, "Server creation/initialization exception: ");
            }
        }
    }
}