using Sila2.Utils;

namespace Biotek.ServerApp
{
    using CommandLine;
    [Verb("biotek", HelpText = "Params for the Biotek Server")]
    public class BiotekCmdLineArgs : ServerCmdLineArgs
    {
        [Option('t', "type", Required = true, HelpText = "The name of the instrument type to control")]
        public string InstrumentType { get; set; }
        [Option("washer-com-port", Required = true, HelpText = "The communication port to connect to washer")]
        public string WasherComPort { get; set; }
        [Option("stacker-com-port", Required = false, HelpText = "The communication port to connect to stacker")]
        public string StackerComPort { get; set; }
        
        [Option('d', "dll-path", Required = false, HelpText = "The path to the folder containing BTILHCRunner.dll")]
        public string DllPath { get; set; }
        [Option("log-dir", Required = false, HelpText = "The path to the directory to save the application logs")]
        public string LogDir { get; set; }
    }
}