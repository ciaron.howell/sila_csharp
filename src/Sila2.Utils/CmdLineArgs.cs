using System;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using CommandLine;
using Common.Logging;

namespace Sila2.Utils
{
    public class CmdLineArgs
    {
        private static readonly ILog Log = LogManager.GetLogger<CmdLineArgs>();

        /// <summary>
        /// Utility function for Servers to use in their CLI applications to parse cmd line arguments
        /// </summary>
        /// <param name="args"></param>
        /// <typeparam name="TCmdLineArgs">Must be a command line argument object which is or either extends <see cref="CmdLineArgs"/></typeparam>
        /// <returns></returns>
        public static TCmdLineArgs Parse<TCmdLineArgs>(string[] args)
            where TCmdLineArgs : ServerCmdLineArgs
        {
            TCmdLineArgs options;
            if (args.Length == 0)
            {
                try
                {
                    options = ParseFromFile<TCmdLineArgs>();
                }
                catch (FileLoadException e)
                {
                    Log.Error(e.Message);
                    options = ParseCmdLineArgs<TCmdLineArgs>(args);
                }
            }
            else
            {
                options = ParseCmdLineArgs<TCmdLineArgs>(args);
            }

            return options;
        }

        private static TCmdLineArgs ParseFromFile<TCmdLineArgs>()
            where TCmdLineArgs : ServerCmdLineArgs
        {
            TCmdLineArgs options;
            try
            {
                IConfigWrapper<TCmdLineArgs> configWrapper =
                    new PersistentConfigWrapper<TCmdLineArgs>(EntryAssemblyPath(ServerCmdLineArgs.AppConfigFile));
                options = configWrapper.GetConfig();
            }
            catch (Exception e)
            {
                throw new FileLoadException($"Could not load config from {EntryAssemblyPath(ServerCmdLineArgs.AppConfigFile)}, you must start the server with the required command line options and '-w' at least once", e);
            }

            return options;
        }

        private static TCmdLineArgs ParseCmdLineArgs<TCmdLineArgs>(string[] args)
            where TCmdLineArgs : ServerCmdLineArgs
        {
            TCmdLineArgs options = null;
            var parser = new Parser(with => with.AutoVersion = false);
            parser.ParseArguments<TCmdLineArgs>(args)
                .WithParsed(o =>
                {
                    options = o;
                    if (!options.Version) return;
                    DebugInfo.PrintVersion();
                    Environment.Exit(0);
                })
                .WithParsed(o =>
                {
                    Log.Debug("Checking interface list flag");
                    options = o;
                    if (!options.ListInterfaces) return;
                    DebugInfo.PrintNetworkInterfaces();
                    Environment.Exit(0);
                })
                .WithParsed(o =>
                {
                    options = o;
                    if (options.Save)
                    {
                        Log.Debug("Saving configuration data");
                        IConfigWrapper<TCmdLineArgs> configWrapper =
                            new PersistentConfigWrapper<TCmdLineArgs>(EntryAssemblyPath(ServerCmdLineArgs.AppConfigFile));
                        configWrapper.SetConfig(options);
                    }
                })
                .WithNotParsed(HandleError);
            return options;
        }

        private static void HandleError(IEnumerable<Error> errs)
        {
            Environment.Exit(1);
        }

        private static string EntryAssemblyPath(string file)
        {
            return Path.Combine(Assembly.GetEntryAssembly().Location, "..", file);
        }
    }

    [Verb("debug-info", HelpText = "Lists debug information such as list of available network interfaces")]
    public class DebugInfo
    {
        /// <summary>
        /// Prints the list of network interfaces available on the system
        /// </summary>
        public static void PrintNetworkInterfaces()
        {
            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            Console.WriteLine("Available Network Interfaces:");
            foreach (var item in interfaces)
            {
                Console.WriteLine($"{item.Name}");
                Console.WriteLine($"     {item.Id}");
                Console.WriteLine($"     {item.NetworkInterfaceType}");
                Console.WriteLine($"     {item.GetPhysicalAddress()}");
                Console.WriteLine($"     {item.OperationalStatus}");
                var addresses = "";
                foreach (var ip in item.GetIPProperties().UnicastAddresses)
                {
                    addresses += $"{ip.Address} , ";
                }

                Console.WriteLine($"     {addresses}");
            }
        }

        /// <summary>
        /// Gets the git hash value from the assembly or null if it cannot be found. 
        /// </summary>
        /// <returns></returns>
        public static void PrintVersion()
        {
            var asm = typeof(Server.SiLA2Server).Assembly;
            var attrs = asm.GetCustomAttributes<AssemblyMetadataAttribute>();
            var version = attrs.FirstOrDefault(a => a.Key == "GitHash")?.Value;
            Console.Out.WriteLine($"{version}");
        }
    }
}