namespace Sila2.Utils
{
    using CommandLine;
    using System.Runtime.Serialization;

    public class ServerCmdLineArgs
    {
        public const string AppConfigFile = "app_config.json";

        [Option("version", Default = false, Required = false,
            HelpText = "Displays version information")]
        public bool Version { get; set; }
        
        [DataMember]
        [Option('p', "port", Default = 50052, HelpText = "Port to connect server to.")]
        public int Port { get; set; }

        [DataMember]
        [Option('c', "config", Required = false, HelpText = "Path to file to store/load server configuration.")]
        public string ConfigFile { get; set; }

        [DataMember]
        [Option('n', "network-interface", Required = false,
            HelpText =
                "Network interface/IP Address/ Mask to use for discovery (e.g. lo, wlp1s0, eth0, 192.168.1.0/24, 192.168.1.0)")]
        public string InterfaceNameOrCIDR { get; set; }

        [DataMember]
        [Option('s', "simulation", Default = false, Required = false, HelpText = "Run the server in simulation")]
        public bool Simulation { get; set; }

        [DataMember]
        [Option('l', "list-interfaces", Default = false, Required = false,
            HelpText = "List available network interfaces")]
        public bool ListInterfaces { get; set; }

        [Option('w', "save", Default = false, Required = false,
            HelpText = "Save the command line arguments to config.json in path of executable")]
        public bool Save { get; set; }
    }
}