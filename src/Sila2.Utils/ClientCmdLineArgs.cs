namespace Sila2.Utils
{
    using CommandLine;
    using Common.Logging;

    public class ClientCmdLineArgs
    {
        [Option('p', "port", Required = false, Default = -1, HelpText = "Port where the service is open")]
        public int Port { get; set; }

        [Option('i', "ip-address", Required = false, Default = null, HelpText = "Ip address of the server")]
        public string IpAddress { get; set; }

        public static void HandleParseError(object errs)
        {
            throw new ClientCmdLineArgsParseException("Failed to parse arguments correctly, try with --help flag");
        }
    }

    public class ClientCmdLineArgsParseException : System.Exception
    {
        public ClientCmdLineArgsParseException() { }
        public ClientCmdLineArgsParseException(string message) : base(message) { }
    }
}