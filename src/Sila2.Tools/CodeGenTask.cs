﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Sila2.Tools
{
    public class CodeGenTask : Task
    {
        [Required] public string FeatureFile { get; set; }
        [Required] public string OutputDir { get; set; }
        [Output] public TaskItem ProtoFile { get; set; }

        private readonly string CodeGenerator;
        private readonly string ProjectDirectory;
        private readonly string _assemblyDirectory;

        public CodeGenTask()
        {
            ProjectDirectory = Directory.GetCurrentDirectory();
            _assemblyDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var osPlatform = GetOSPlatform();
            CodeGenerator = Path.Combine($"{_assemblyDirectory}", "..", "code_generator.jar");
            ThrowIfNotExists(CodeGenerator);
        }

        private static OSPlatform GetOSPlatform()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                return OSPlatform.Linux;
            }

            return RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? OSPlatform.Windows : new OSPlatform();
        }

        public override bool Execute()
        {
            LogProperties();
            //TODO: Check for system dependencies (if it works)
            return GenerateProto(FeatureFile);
        }

        private void LogProperties()
        {
            Log.LogMessage(MessageImportance.Low, $"OutputDir: {OutputDir}");
            Log.LogMessage(MessageImportance.Low, $"ProjectDirectory: {ProjectDirectory}");
            Log.LogMessage(MessageImportance.Low, $"CodeGenerator: {CodeGenerator}");
        }

        /// <summary>
        /// Checks for system dependencies if on non x64 OS
        ///
        /// TODO: this is currently not working as Environment.Is64BitOperatingSystem returns false when
        ///       on a x64 OS so lets not use it.
        /// </summary>
        /// <exception cref="Exception"></exception>
        private string GetJavaExecutablePath()
        {
            var osPlatform = GetOSPlatform();

            if (osPlatform == OSPlatform.Windows)
            {
                return Path.Combine($"{_assemblyDirectory}", "..", "code_generator-win-x86", "jre", "bin", "java.exe");
            }

            if (osPlatform == OSPlatform.Linux)
            {
                return Path.Combine($"{_assemblyDirectory}", "..", "code_generator-linux-x64", "jre", "bin", "java");
            }
            
            throw new Exception($"Sila2.Tools may not support code generation for your {osPlatform} platform unless you have Java 8 or greater on your PATH");
        }

        private bool GenerateProto(string featureFile)
        {
            try
            {
                var featureAbsolutePath = Path.Combine(ProjectDirectory, featureFile);
                ThrowIfNotExists(featureAbsolutePath);

                var featureFileName = Path.GetFileNameWithoutExtension(featureAbsolutePath);
                var protoFile = featureFileName.Replace("sila", "proto");
                var protoPath = Path.Combine(OutputDir, protoFile);
                ProtoFile = new TaskItem(protoFile);

                var codeGenArguments = new StringBuilder();
                codeGenArguments.Append($"-jar {CodeGenerator} ");
                codeGenArguments.Append($"{featureAbsolutePath} ");
                codeGenArguments.Append($"{protoPath}");

                var javaPath = "java";
                try
                {
                    javaPath = GetJavaExecutablePath();
                }
                catch (Exception e)
                {
                    Log.LogWarning(e.Message);
                }

                var psi = new ProcessStartInfo
                {
                    FileName = javaPath,
                    Arguments = codeGenArguments.ToString(),
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                };

                var process = new Process
                {
                    StartInfo = psi
                };

                Log.LogMessage(MessageImportance.Low, $"{psi.FileName} {psi.Arguments}");
                try
                {
                    process.Start();
                    process.WaitForExit();
                }
                catch (Exception e)
                {
                    throw new Exception($"Failed to generate feature {featureFile} with {javaPath}: {e.Message}\n${process.StandardError.ReadToEnd()}");
                }

                if (process.ExitCode == 0)
                {
                    Log.LogMessage(MessageImportance.High,
                        $"{Path.GetFileName(featureFile)} -> {Path.Combine(ProjectDirectory, protoPath)}");
                }
                else
                {
                    Log.LogError($"Failed to generate feature {featureFile}:\n{process.StandardError.ReadToEnd()}");
                }

                return process.ExitCode == 0;
            }
            catch (Exception e)
            {
                Log.LogError($"{e.Message}");
                return false;
            }
        }

        private static void ThrowIfNotExists(string featureAbsolutePath)
        {
            if (!File.Exists(featureAbsolutePath))
            {
                throw new FileNotFoundException($"File {featureAbsolutePath} does not exist");
            }
        }

        private static void CheckJavaIsAvailable()
        {
            var psi = new ProcessStartInfo
            {
                FileName = "java",
                UseShellExecute = false,
                Arguments = " -version",
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };

            var process = new Process
            {
                StartInfo = psi
            };

            process.Start();
            process.WaitForExit();

            const string detailedMessage =
                "Please install Java 8 or greater or add it to your system PATH if you would like to to use the Feature to stub generator as a build step";
            if (process.ExitCode != 0)
            {
                throw new Exception(
                    $"There is no Java installation found on your machine\n{detailedMessage}");
            }

            var output = process.StandardError.ReadToEnd();
            var java8PlusRegex = new Regex("^java version \"1\\.(\\d+)");
            var match = java8PlusRegex.Match(output);
            if (!match.Success)
            {
                throw new Exception(
                    $"There is no Java installation found on your machine\n{detailedMessage}");
            }

            var groups = match.Groups;
            var versionNumber = short.Parse(groups[1].Value);
            if (versionNumber < 8)
            {
                throw new Exception(
                    $"Incorrect version of Java found on your machine: {groups[0]}\n{detailedMessage}");
            }
        }
    }
}