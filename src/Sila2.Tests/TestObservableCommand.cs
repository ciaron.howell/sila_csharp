using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Examples.Testfeature.V1;
using Sila2.Utils;
using Xunit;
using Xunit.Abstractions;

namespace Sila2.Server.Command.Observable.Test
{
    public class TestObservableCommand : IDisposable
    {
        public const int TaskDuration = 1000; // [ms]
        public const int Timeout = TaskDuration * 10; // [ms]
        public const int LifetimeOfExecution = TaskDuration * 3; // [ms]
        public const int ServerPort = 50053;
        public const string ServerIp = "0.0.0.0";
        private static readonly ILog _log = LogManager.GetLogger<TestObservableCommand>();
        private readonly ITestOutputHelper _output;
        private readonly Grpc.Core.Server _grpcServer = new Grpc.Core.Server();
        private readonly Grpc.Core.Channel _channel;
        private readonly TestFeature.TestFeatureClient _client;

        private TestFeatureImpl _testFeatureImpl =
            new TestFeatureImpl(TimeSpan.FromMilliseconds(LifetimeOfExecution));

        public TestObservableCommand(ITestOutputHelper output)
        {
            Logging.SetupCommonLogging();
            _output = output;
            Tests.Common.Logging.SetupLogForwarding(_output);
            _grpcServer.Services.Add(TestFeature.BindService(_testFeatureImpl));
            _grpcServer.Ports.Add(new ServerPort(ServerIp, ServerPort, ServerCredentials.Insecure));
            _grpcServer.Start();
            _channel = new Channel($"{ServerIp}:{ServerPort}", ChannelCredentials.Insecure);
            _client = new TestFeature.TestFeatureClient(_channel);
        }

        /// <summary>
        /// Test a task error on execution 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestFailedCommandExecution()
        {
            // Test error on the execution of the 
            _testFeatureImpl.TestObservableFunc = FailedFunc(TaskDuration);
            var (call, commandConfirmation) = CallObservableAndInfo();

            var statuses = new List<ExecutionInfo.Types.CommandStatus>
                {ExecutionInfo.Types.CommandStatus.Running, ExecutionInfo.Types.CommandStatus.FinishedWithError};

            await AwaitAndAssert(call, statuses);

            Assert.Throws<RpcException>(() => _client.TestObservable_Result(commandConfirmation.CommandExecutionUUID));
        }

        /// <summary>
        /// Test a successfully executed
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestSuccessfulCommandExecution()
        {
            // Test error on the execution of the 
            _testFeatureImpl.TestObservableFunc = SuccessfulFunc(TaskDuration);
            var (call, _) = CallObservableAndInfo();

            var statuses = new List<ExecutionInfo.Types.CommandStatus>
                {ExecutionInfo.Types.CommandStatus.Running, ExecutionInfo.Types.CommandStatus.FinishedSuccessfully};
            await AwaitAndAssert(call, statuses);
        }


        /// <summary>
        /// Test two client streams connected and one client going down. 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestSuccessfulCommandExecutionWith2ClientsWith1FailedClient()
        {
            // Test error on the execution of the 
            _testFeatureImpl.TestObservableFunc = SuccessfulFunc(TaskDuration);
            var (call, commandConfirmation) = CallObservableAndInfo();
            var call2 = _client.TestObservable_Info(commandConfirmation.CommandExecutionUUID);

            var statuses = new List<ExecutionInfo.Types.CommandStatus>
                {ExecutionInfo.Types.CommandStatus.Running, ExecutionInfo.Types.CommandStatus.FinishedSuccessfully};
            var assertionTask = AwaitAndAssert(call, statuses);

            // Disconnect the second stream
            call2.Dispose();
            await assertionTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestCommandLifetimeExpiration()
        {
            var taskDuration = TaskDuration;
            ValidateTaskDuration(taskDuration);

            _testFeatureImpl.TestObservableFunc = SuccessfulFunc(taskDuration);
            var commandConfirmation = _client.TestObservable(new TestObservable_Parameters(), null, null);
            var tokenSource = new CancellationTokenSource(Timeout);

            try
            {
                await Task.Delay(LifetimeOfExecution + 2000);
                tokenSource.Token.ThrowIfCancellationRequested();

                var call = _client.TestObservable_Info(commandConfirmation.CommandExecutionUUID);
                while (await call.ResponseStream.MoveNext(tokenSource.Token))
                {
                    await Task.Delay(500);
                }
            }
            catch (RpcException e)
            {
                // "This is not a SiLA error"
                Assert.Equal(StatusCode.Aborted, e.StatusCode);
                var silaError = ErrorHandling.RetrieveSiLAError(e);
                Assert.NotNull(silaError.FrameworkError);
                Assert.Equal(silaError.FrameworkError.ErrorType,
                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid);
                return;
            }

            throw new Exception("Test did not validate anything!");
        }

        /// <summary>
        /// Test that the Execution info stream errors out if command has expired when providing response
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestInfoErrorOnLifetimeExpiration()
        {
            var taskDuration = TaskDuration;
            ValidateTaskDuration(taskDuration);

            _testFeatureImpl.TestObservableFunc = SuccessfulFunc(taskDuration);
            var (call, commandConfirmation) = CallObservableAndInfo();
            var tokenSource = new CancellationTokenSource(Timeout);

            try
            {
                await Task.Delay(LifetimeOfExecution + 2000);
                tokenSource.Token.ThrowIfCancellationRequested();

                while (await call.ResponseStream.MoveNext(tokenSource.Token))
                {
                    await Task.Delay(500, tokenSource.Token);
                }
            }
            catch (RpcException e)
            {
                // This is not a SiLAFramework.SiLAError but a SiLAFramework.FrameworkError
                Assert.Equal(StatusCode.Aborted, e.StatusCode);
                var silaError = ErrorHandling.RetrieveSiLAError(e);
                Assert.NotNull(silaError.FrameworkError);
                Assert.Equal(                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid,
silaError.FrameworkError.ErrorType);
                return;
            }

            throw new Exception("Test did not validate anything!");
        }

        private static int ValidateTaskDuration(int taskDuration)
        {
            // Test error on the execution of the 
            const int extendedLifetime = LifetimeOfExecution * 2;
            var maxCommandLifetime =
                taskDuration > LifetimeOfExecution ? extendedLifetime : LifetimeOfExecution;
            Assert.True(Timeout > maxCommandLifetime,
                $"The timeout is shorter than the maximum possible lifetime of the command, " +
                $"timeout: {Timeout}, Command lifetime: {maxCommandLifetime}. Cannot execute this test!");
            return taskDuration;
        }

        private static async Task AwaitAndAssert(AsyncServerStreamingCall<ExecutionInfo> call,
            IEnumerable<ExecutionInfo.Types.CommandStatus> commandStatuses)
        {
            foreach (var commandStatus in commandStatuses)
            {
                await AwaitUntil(call, commandStatus);
                Assert.Equal(commandStatus, call.ResponseStream.Current.CommandStatus);
            }
        }

        /// <summary>
        /// Creates a client, issues an observable command and returns created elements
        /// </summary>
        /// <returns></returns>
        private (AsyncServerStreamingCall<ExecutionInfo>, CommandConfirmation) CallObservableAndInfo()
        {
            var commandConfirmation = _client.TestObservable(new TestObservable_Parameters(), null, null);
            var call = _client.TestObservable_Info(commandConfirmation.CommandExecutionUUID);
            return (call, commandConfirmation);
        }

        private static async Task AwaitUntil(AsyncServerStreamingCall<ExecutionInfo> call,
            ExecutionInfo.Types.CommandStatus commandStatus)
        {
            var token = new CancellationTokenSource(Timeout);
            while (await call.ResponseStream.MoveNext(token.Token) &&
                   call.ResponseStream.Current.CommandStatus != commandStatus)
            {
                _log.Warn($"Awaiting {commandStatus}, current status {call.ResponseStream.Current.CommandStatus}");
                await Task.Delay(Constants.AWAIT_PERIOD);
            }
        }


        /// TODO: Test for cancelling task and stream
        private static Func<IProgress<ExecutionInfo>, TestObservable_Parameters,
                CancellationToken, TestObservable_Responses>
            SuccessfulFunc(int taskDuration)
        {
            return (progress, parameters, cancellationToken) =>
            {
                _log.Info("Starting task...");
                Task.Delay(taskDuration, cancellationToken).Wait(cancellationToken);
                var response = new TestObservable_Responses();
                _log.Info("Finishing task!");
                return response;
            };
        }

        /// <summary>
        /// Task simulating internal driver error when interacting with instrument
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        private static Func<IProgress<ExecutionInfo>, TestObservable_Parameters, CancellationToken, TestObservable_Responses>
            FailedFunc(int taskDuration)
        {
            return (progress, parameters, token) =>
            {
                _log.Info("Starting task...");
                Task.Delay(taskDuration).Wait(token);
                throw new NotSupportedException("Task execution error!");
            };
        }

        public async void Dispose()
        {
            _log.Warn("Killing gRPC server ...");
            await _grpcServer.KillAsync();
        }
    }
}