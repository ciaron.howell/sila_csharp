namespace Sila2.Utils
{
    using System;
    using Google.Protobuf;
    using Grpc.Core;
    using SiLAFramework = Org.Silastandard;

    /// <summary>
    /// Provides methods to generate SiLA errors and raise gRPC exceptions and to handle those execptions.
    /// </summary>
    public class ErrorHandling
    {
        /// <summary>
        /// Raise a SiLA error by throwing an exception containing the given SiLAError object.
        /// </summary>
        /// <param name="silaError">The SiLAError object to be thrown.</param>
        public static void RaiseSiLAError(SiLAFramework.SiLAError silaError)
        {
            throw new RpcException(new Status(StatusCode.Aborted, Convert.ToBase64String(silaError.ToByteArray())));
        }

        /// <summary>
        /// Returns a SiLA DefinedExecutionError object assembled from the 
        /// given DefinedExecutionError identifier and the error message.
        /// </summary>
        /// <param name="errorIdentifier">The DefinedExecutionError identifier as defined by the according feature definition.</param>
        /// <param name="message">The error message.</param>
        /// <returns>The created SiLA DefinedExecutionError object.</returns>
        public static SiLAFramework.SiLAError CreateDefinedExecutionError(string errorIdentifier, string message)
        {
            return new SiLAFramework.SiLAError
            {
                DefinedExecutionError = new SiLAFramework.DefinedExecutionError
                {
                    ErrorIdentifier = errorIdentifier,
                    Message = message
                }
            };
        }

        /// <summary>
        /// Returns a SiLA UndefinedExecutionError object assembled from the 
        /// given error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The created SiLA UndefinedExecutionError object.</returns>
        public static SiLAFramework.SiLAError CreateUndefinedExecutionError(string message)
        {
            return new SiLAFramework.SiLAError
            {
                UndefinedExecutionError = new SiLAFramework.UndefinedExecutionError
                {
                    Message = message
                }
            };
        }

        /// <summary>
        /// Returns a SiLA ValidationError object assembled from the 
        /// given parameter identifier and the error message.
        /// </summary>
        /// <param name="parameter">The name of the parameter that caused the error.</param>
        /// <param name="message">The error message.</param>
        /// <returns>The created SiLA ValidationError object.</returns>
        public static SiLAFramework.SiLAError CreateValidationError(string parameter, string message)
        {
            return new SiLAFramework.SiLAError
            {
                ValidationError = new SiLAFramework.ValidationError
                {
                    Parameter = parameter,
                    Message = message
                }
            };
        }

        /// <summary>
        /// Returns a SiLA FrameworkError object assembled from the 
        /// given error type and the error message.
        /// </summary>
        /// <param name="errorType">The type of FrameworkError (as defined in the SiLA standard.</param>
        /// <param name="message">The error message.</param>
        /// <returns>The created SiLA FrameworkError object.</returns>
        public static SiLAFramework.SiLAError CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType errorType, string message)
        {
            return new SiLAFramework.SiLAError
            {
                FrameworkError = new SiLAFramework.FrameworkError
                {
                    ErrorType = errorType,
                    Message = message
                }
            };
        }

        /// <summary>
        /// Delivers a string presenting the content of a given exception caught in the SiLA context
        /// (may contain a gRPC ecxception and a SiLAError object) that can be used e.g. for logging.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>The string</returns>
        public static string HandleException(Exception e)
        {
            RpcException rpcEx = e is RpcException exception ? exception : e.InnerException as RpcException;
            if (rpcEx != null)
            {
                if (rpcEx.Status.StatusCode == StatusCode.Aborted && !string.IsNullOrEmpty(rpcEx.Status.Detail))
                {
                    // check if SiLAError object is contained
                    try
                    {
                        var error = RetrieveSiLAError(rpcEx);
                        return $"SiLA {GetSiLAErrorType(error)} Error occured. Details: {error}";
                    }
                    catch (Exception)
                    {
                        // any other gRPC error => SiLA Connection Error
                        return "SiLA Connection Error occured: " + rpcEx.Message;
                    }
                }
                else
                {
                    return "Non-SiLA gRPC exception occured: " + rpcEx.Message;
                }
            }

            return "Exception occured: " + FlattenExceptionsMessageToString(e);
        }

        /// <summary>
        /// Recusrsively parses the content of a given exception and possible contained 
        /// inner exceptions into one string (e.g. for logging all exception information).
        /// </summary>
        /// <param name="e">The exception to be parsed.</param>
        /// <returns>A string containing all exception information.</returns>
        public static string FlattenExceptionsMessageToString(Exception e)
        {
            if (e is AggregateException)
            {
                if (e.InnerException == null)
                {
                    return string.Empty;
                }
                return FlattenExceptionsMessageToString(e.InnerException);
            }

            if (e.InnerException == null)
            {
                return e.GetType().Name + ": " + e.Message;
            }

            return e.GetType().Name + ": " + e.Message + Environment.NewLine + FlattenExceptionsMessageToString(e.InnerException);
        }

        /// <summary>
        /// Parse the contained SiLAError object from a the given gRPC exception.
        /// </summary>
        /// <param name="e">The gRPC exception containg a SiLA error.</param>
        /// <returns>The SiLAError object.</returns>
        public static SiLAFramework.SiLAError RetrieveSiLAError(RpcException e)
        {
            return SiLAFramework.SiLAError.Parser.ParseFrom(Convert.FromBase64String(e.Status.Detail));
        }

        private static string GetSiLAErrorType(SiLAFramework.SiLAError error)
        {
            switch (error.ErrorCase)
            {
                case SiLAFramework.SiLAError.ErrorOneofCase.DefinedExecutionError:
                    return ("DefinedExecution");
                case SiLAFramework.SiLAError.ErrorOneofCase.UndefinedExecutionError:
                    return ("UndefinedExecution");
                case SiLAFramework.SiLAError.ErrorOneofCase.FrameworkError:
                    return ("Framework");
                case SiLAFramework.SiLAError.ErrorOneofCase.ValidationError:
                    return ("Validation");
            }

            return (null);
        }
    }
}