namespace Sila2.Utils
{
    public class NetworkInterfaceNotFoundException : System.Exception
    {
        public NetworkInterfaceNotFoundException (string message) : base (message)
        { }
    }
}