﻿namespace Sila2.Utils
{
    using System.Collections.Generic;
    using Grpc.Core;
    using Org.Silastandard;

    /// <summary>
    /// Utility class that provides several static methods to handle SiLA Client Metadata.
    /// </summary>
    public class SilaClientMetadata
    {
        private const string Prefix = "sila-";
        private const string Suffix = "-bin";

        /// <summary>
        /// Returns a list of all SiLA Client Metadata Identifiers contained in the given Metadata
        /// </summary>
        /// <param name="metadata">The gRPC metadata as passed within the request headers of a SiLA client request.</param>
        /// <returns>List of all found SiLA Client Metadata Identifiers.</returns>
        public static List<string> GetAllSilaClientMetadataIdentifiers(Metadata metadata)
        {
            List<string> identifiers = new List<string>();

            foreach (var entry in metadata)
            {
                if (entry.Key.StartsWith(Prefix) && entry.Key.EndsWith(Suffix))
                {
                    identifiers.Add(entry.Key.Substring(Prefix.Length, entry.Key.Length - Prefix.Length - Suffix.Length));
                }
            }

            return identifiers;
        }

        /// <summary>
        /// Returns the value of the metadata entry for which the key matches the given fully qualified metadata identifier
        /// or null if the given key is not found.
        /// </summary>
        /// <param name="metadata">The gRPC metadata as passed within the request headers of a SiLA client request.</param>
        /// <param name="fullyQualifiedMetadataIdentifier">The fully qualified identifier of the metadata to be found.</param>
        /// <returns>The metadata value as byte array.</returns>
        public static byte[] GetSilaClientMetadataValue(Metadata metadata, string fullyQualifiedMetadataIdentifier)
        {
            foreach (var entry in metadata)
            {
                if (entry.Key.Equals(ConvertMetadataIdentifierToWireFormat(fullyQualifiedMetadataIdentifier)))
                {
                    return entry.ValueBytes;
                }
            }

            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, $"No metadata '{fullyQualifiedMetadataIdentifier}' found in client request"));
            return null;
        }

        /// <summary>
        /// Converts the given fully qualified metadata identifier into the wire format to be sent as metadata key within the gRPC request headers according to the SiLA standard.
        /// </summary>
        /// <param name="fullyQualifiedMetadataIdentifier"></param>
        /// <returns>The correct format of a fully qualified metadata identifier to be sent with a gRPC request.</returns>
        public static string ConvertMetadataIdentifierToWireFormat(string fullyQualifiedMetadataIdentifier)
        {
            if (string.IsNullOrEmpty(fullyQualifiedMetadataIdentifier))
            {
                return string.Empty;
            }

            return Prefix + fullyQualifiedMetadataIdentifier.Replace('/', '-').ToLower() + Suffix;
        }
    }
}