﻿namespace Sila2.Discovery
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Threading.Tasks;
    using Common.Logging;
    using Grpc.Core;
    using Makaretu.Dns;
    using Utils;

    /// <summary>
    /// Class to query for sila services via mDNS queries.
    /// </summary>
    public static class ServiceFinder
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ServiceFinder).FullName);

        /// <summary>
        /// Queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="timeout">Max time (in milli-seconds) to discover </param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static List<Channel> QueryAllServices(int timeout = 10000)
        {
            return QueryAllServicesAsync(timeout).Result;
        }

        /// <summary>
        /// Asynchronously queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static async Task<List<Channel>> QueryAllServicesAsync(int time = 10000, params NetworkInterface[] interfaceFilter)
        {
            // list of services found during the browsing time
            var services = new List<Channel>();

            using (var mdns = new MulticastService(Networking.CreateFilterFunc(interfaceFilter)))
            {
                // attach the event handler for processing responses.
                mdns.AnswerReceived += (s, e) =>
                {
                    // The first Answer will be a PTRRecord
                    var pointers = e.Message.Answers.OfType<PTRRecord>().Where(p => p.Name.Contains(DiscoveryConstants.ServiceName));
                    foreach (var pointer in pointers)
                    {
                        // Ask for the service instance details
                        mdns.SendQuery(pointer.DomainName, type: DnsType.SRV);
                    }

                    // The second Answer will be a SRVRecord (also contain A and AAAA records)
                    var servers = e.Message.Answers.OfType<SRVRecord>().Where(p => p.Name.Contains(DiscoveryConstants.ServiceName));
                    foreach (var server in servers)
                    {
                        var A_record = e.Message.AdditionalRecords.OfType<ARecord>().FirstOrDefault();
                        if (A_record != null)
                        {
                            services.Add(BuildNewChannel(A_record.Address, server.Port));
                        }
                    }
                };

                mdns.Start();
                // Ask for PTRRecords (the devices will only respond to SRV that are directed exactly to them)
                mdns.SendQuery(string.Join(".", DiscoveryConstants.ServiceName, DiscoveryConstants.ServiceDomainName), type: DnsType.PTR);
                await Task.Delay(time);
                mdns.Stop();
            }

            return services;
        }

        /// <summary>
        /// Utility function to create gRPC channel from IP and port from ServerDiscoveryInfo
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static Channel BuildNewChannel(IPAddress host, int port)
        {
            var channel = new Channel($"{host}:{port}", ChannelCredentials.Insecure);
            return channel;
        }
    }
}