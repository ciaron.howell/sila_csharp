﻿namespace Sila2
{
    using Server;

    public interface IFeatureImplementation
    {
        Feature SiLAFeature { get; set; }

        SiLA2Server SiLAServer { get; set; }
    }
}